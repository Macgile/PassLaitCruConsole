﻿using PassDatabase.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace PassLaitCru
{
    internal class Program
    {
        #region Methods

        private static void Main()
        {
            var objectifs = new List<Objectif>();
            const StringComparison ignoreCase = StringComparison.OrdinalIgnoreCase;

            using (var db = new PassDatabaseEntities())
            {
                // var complement = db.COMPLEMENT.Where(o => o.ID_OBJECTIF == 4).ToList();
                // var contenu = db.CONTENU.ToList();
                //var objs = db.OBJECTIF.Where(o => o.ID == 4).ToList();

                var objs = db.OBJECTIF.ToList();

                foreach (var o in objs)
                {
                    var obj = new Objectif
                    {
                        Nom = o.NOM,
                        Id = (int) o.ID
                    };

                    foreach (var comp in o.COMPLEMENT)
                    {
                        var complement = new Complement
                        {
                            Nom = comp.NOM,
                            Id = (int) comp.ID
                        };

                        foreach (var grp in comp.CONTENU.GroupBy(g => g.GROUPE).Select(g => g))
                        {
                            var list = comp.CONTENU.Where(g => g.GROUPE == grp.Key)
                                .Select(c => new Contenu
                                {
                                    Id = (int) c.ID,
                                    Description = c.DESCRIPTION,
                                    Groupe = c.GROUPE ?? 0
                                })
                                .ToList();

                            complement.Contenus.Add(list);
                        }
                        obj.Add(complement);
                    }
                    objectifs.Add(obj);
                }

                Debug.WriteLine("");
            }

            var optitraite =
                objectifs.FirstOrDefault(o => o.Nom.Equals("Qualité de l'alimentation solide", ignoreCase));
            var qualiteAlimentation = optitraite?.Complements.FirstOrDefault(c => c.Id == 13);
            var dacPropre = optitraite?.Complements.FirstOrDefault(c => c.Id == 14);
            var dacNettoyable = optitraite?.Complements.FirstOrDefault(c => c.Id == 15);

            var conte = qualiteAlimentation?.Contenus;//          optitraite?.Complements.First();
            var last = optitraite?.Complements.Last();
            //var firstFirst = conte?.Where(f => f.Where(f => f.Groupe == 1).Select(f => f).Select(c => c);

            var q_ualiteAlimentation = optitraite?.Complements
                .FirstOrDefault(c => c.Nom.Equals("Qualité de l’alimentation solide", ignoreCase))?
                .Contenus;
            //var contenu = complements?.Count; //13 "Qualité de l’alimentation solide" 14 "Tables / mangeoires / auges / DAC / support bloc propres " 15 "Tables / mangeoires / auges / DAC nettoyable"
            // CollectionDebugView<PassLaitCru.Complement>(optitraite.Contenus).Items[1]).Nom	"Tables / mangeoires / auges / DAC / support bloc propres "	string
            // 	CollectionDebugView<PassLaitCru.Complement>(optitraite.Contenus).Items[0]).Nom	"Qualité de l’alimentation solide"	string
            // (new System.Collections.Generic.Mscorlib_CollectionDebugView<PassLaitCru.Complement>(optitraite.Contenus).Items[1]).Nom
            Debug.WriteLine("");
        }

        /*
        /// <summary>
        /// https://www.tektutorialshub.com/generic-methods-in-c/
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        private static List<T> GetInitializedList<T>(T value, int id)
        {
            // This generic method returns a List with ten elements initialized.
            // ... It uses a type parameter.
            // ... It uses the "open type" T.
            List<T> list = new List<T>();
            for (int i = 0; i < id; i++)
            {
                list.Add(value);
            }
            return list;
        }
        */

        #endregion Methods
    }
}