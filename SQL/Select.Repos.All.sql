SELECT O.ID AS ID,
       O.NOM AS Objectif,
       CO.NOM AS Complement,
       CO.ID AS Complement_ID,
       C.DESCRIPTION AS Contenu,
       C.GROUPE
  FROM OBJECTIF O
       INNER JOIN
       COMPLEMENT CO ON O.ID = CO.ID_OBJECTIF
       INNER JOIN
       CONTENU C ON CO.ID = C.ID_COMPLEMENT;
