//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace PassDatabase.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class COMPLEMENT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public COMPLEMENT()
        {
            this.CONTENU = new HashSet<CONTENU>();
            this.CONTROLE_CONTENU = new HashSet<CONTROLE_CONTENU>();
        }
    
        public long ID { get; set; }
        public string NOM { get; set; }
        public Nullable<long> ID_OBJECTIF { get; set; }
    
        public virtual OBJECTIF OBJECTIF { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONTENU> CONTENU { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CONTROLE_CONTENU> CONTROLE_CONTENU { get; set; }
    }
}
