﻿using WPFModern;

// ReSharper disable UnusedAutoPropertyAccessor.Global

// ReSharper disable MemberCanBePrivate.Global

// https://stackoverflow.com/questions/2698910/c-sharp-generics-with-mvvm-pulling-the-t-out-of-t

namespace WPFPassLaitCru.ViewModel
{
    public abstract class ViewModel<TModel> : NotifyPropertyChanged
    {
        #region Constructors

        protected ViewModel(TModel model)
        {
            Model = model;
        }

        #endregion Constructors

        #region Properties

        public TModel Model { get; private set; }

        #endregion Properties
    }
}