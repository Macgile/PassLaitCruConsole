﻿using System;

// ReSharper disable ConvertToAutoProperty
// ReSharper disable ArrangeAccessorOwnerBody

namespace WPFPassLaitCru.ViewModel
{
    public class CustomEventArgs : EventArgs
    {
        #region Private Fields

        private readonly string msg;

        #endregion Private Fields

        #region Public Constructors

        public CustomEventArgs(string s)
        {
            msg = s;
        }

        #endregion Public Constructors

        #region Public Properties

        public string Origine
        {
            get { return msg; }
        }

        #endregion Public Properties
    }
}
