﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using WPFModern;

// ReSharper disable UnusedMember.Global

// ReSharper disable ArrangeAccessorOwnerBody
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnassignedGetOnlyAutoProperty

namespace WPFPassLaitCru.ViewModel
{
    public sealed class ComplementViewModel : NotifyPropertyChanged
    {
        #region Private Fields

        private readonly int idRealised;

        private int itemSelected;

        #endregion Private Fields

        #region Public Constructors

        /// <summary>
        /// ComplementViewModel
        /// </summary>
        /// <param name="id"></param>
        /// <param name="contenu">List of objects of type Content</param>
        /// <param name="nom">Name of the object Complement</param>
        public ComplementViewModel(int id, List<Contenu> contenu, string nom = "")
        {
            if (contenu == null) return;

            Id = id;
            NomComplement = nom;
            // The content id that defines the complement as being realized
            idRealised = contenu.First(c => c.Realized).Id;
            // list of Contenu object
            Contenu = new ObservableCollection<Contenu>(contenu);
        }

        #endregion Public Constructors

        #region Public Events

        public event EventHandler<CustomEventArgs> RaiseEvent;

        #endregion Public Events

        #region Public Properties
        public int Id { get; set; }
        public ObservableCollection<Contenu> Contenu { get; private set; }
        public bool IsChanged { get; set; }
        public string NomComplement { get; set; }
        
        public int SelectedItem
        {
            get { return itemSelected; }
            set
            {
                itemSelected = value;
                IsChanged = true;
                OnPropertyChanged("SelectedItem");
                OnRaiseEvent(new CustomEventArgs(NomComplement));
            }
        }

        public bool Status => itemSelected == idRealised;

        #endregion Public Properties

        #region Private Methods

        private void OnRaiseEvent(CustomEventArgs e)
        {
            // Make a temporary copy of the event to avoid possibility of
            // a race condition if the last subscriber unsubscribes
            // immediately after the null check and before the event is raised.
            var handler = RaiseEvent;

            // Event will be null if there are no subscribers
            handler?.Invoke(this, e);
        }

        #endregion Private Methods
    }
}