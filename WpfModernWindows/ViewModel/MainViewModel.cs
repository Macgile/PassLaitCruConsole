﻿using PassDatabase.Models;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using WPFModern;

// ReSharper disable CollectionNeverQueried.Local

// ReSharper disable ArrangeAccessorOwnerBody

namespace WPFPassLaitCru.ViewModel
{
    public class MainViewModel : NotifyPropertyChanged
    {
        #region Private Fields

        private string notification;
        private List<Objectif> objectifs;

        #endregion Private Fields

        #region Public Properties

        public OptitraiteViewModel OptitraiteVm { get; set; }

        public string Notification
        {
            get { return notification; }
            set
            {
                notification = value;
                OnPropertyChanged("Notification");
            }
        }

        #endregion Public Properties

        #region Public Constructors

        public MainViewModel()
        {
        }

        #endregion Public Constructors

        #region Public Methods

        public void GetAll()
        {
            objectifs = new List<Objectif>();

            using (var db = new PassDatabaseEntities())
            {
                // var complement = db.COMPLEMENT.Where(o => o.ID_OBJECTIF == 4).ToList();
                // var contenu = db.CONTENU.ToList();
                //var objs = db.OBJECTIF.Where(o => o.ID == 1).ToList();

                var objs = db.OBJECTIF.ToList();

                foreach (var o in objs)
                {
                    var obj = new Objectif
                    {
                        Nom = o.NOM,
                        Id = (int) o.ID
                    };

                    foreach (var comp in o.COMPLEMENT)
                    {
                        var complement = new Complement
                        {
                            Nom = comp.NOM,
                            Id = (int) comp.ID
                        };

                        foreach (var grp in comp.CONTENU.GroupBy(g => g.GROUPE).Select(g => g))
                        {
                            var list = comp.CONTENU.Where(g => g.GROUPE == grp.Key)
                                .Select(c => new Contenu
                                {
                                    Id = (int) c.ID,
                                    Description = c.DESCRIPTION,
                                    Groupe = c.GROUPE ?? 0,
                                    Realized = c.REALIZED
                                })
                                .ToList();

                            complement.Contenus.Add(list);
                        }
                        obj.Add(complement);
                    }
                    objectifs.Add(obj);
                }

                /*
                1   Optitraite
                2   Trayons
                3   Eau de source pour la vaiselle laitière
                4   Qualité de l'alimentation solide
                5   Local et extérieur du tank
                6   Intérieur du tank
                7   Propreté des vaches
                8   Propreté matériels de distribution eau + aliment
                9   Stockage des aliments
                10  Filtre à lait
                11  Aire de pompage
                12  Sensibilisation
                */

                //var optitraite = objectifs.FirstOrDefault(o => o.Nom.ToLower() == "optitraite");
                var optitraite = objectifs.FirstOrDefault(o => o.Id == 1); // optitraite

                OptitraiteVm = new OptitraiteViewModel(optitraite);

                OnPropertyChanged("OptitraiteVm");

                // fill selected item
                GetControle(1);
                Debug.WriteLine("");
            }
        }

        private void GetSelectedItem(ComplementViewModel complement)
        {
        }


        private void GetControle(int idControle, string siret = "")
        {
            using (var db = new PassDatabaseEntities())
            {
                var controleDb = db.CONTROLE_CONTENU.Where(c => c.ID_CONTROLE == idControle).ToList();

                //var controleDb = db.CONTROLE.FirstOrDefault(c => c.ID == idControle)?.CONTROLE_CONTENU.Select(c => c)
                //    .ToList();
                Debug.WriteLine("");

                if (controleDb.Count <= 0) return;
                {
                    var idContenu = controleDb.FirstOrDefault(c => c.ID_COMPLEMENT == OptitraiteVm.Controle.Id)
                        ?.ID_CONTENU;
                    if (idContenu != null)
                    {
                        OptitraiteVm.Controle.SelectedItem = (int) idContenu;
                    }
                }


                //var control = OptitraiteVm.Controle;
                //var bilan = OptitraiteVm.Bilan;

                //var item = controleDb?.CONTROLE_CONTENU.FirstOrDefault(c => c.ID_COMPLEMENT == control.Id);

                //// 1 || 2

                //OptitraiteVm.Controle.SelectedItem = (int)controleDb?.CONTROLE_CONTENU.FirstOrDefault(c => c.ID_COMPLEMENT == control.Id)?.ID_CONTENU;

                //// 3 || 4
                //OptitraiteVm.Bilan.SelectedItem = (int)controleDb?.CONTROLE_CONTENU.FirstOrDefault(c => c.ID_COMPLEMENT == bilan.Id)?.ID_CONTENU;

                // set selected item with id_contenu in controle result
            }
        }

        #endregion Public Methods
    }
}