﻿using System.Diagnostics;
using System.Linq;
using WPFModern;

// ReSharper disable UnusedAutoPropertyAccessor.Global

// ReSharper disable MemberCanBePrivate.Global

// ReSharper disable ArrangeAccessorOwnerBody
// ReSharper disable AssignNullToNotNullAttribute
// ReSharper disable ConvertToAutoProperty

#pragma warning disable 169

namespace WPFPassLaitCru.ViewModel
{
    public class OptitraiteViewModel : NotifyPropertyChanged
    {
        #region Private Fields

        private readonly ComplementViewModel bilanVm;
        private readonly Complement complement;
        private readonly ComplementViewModel controleVm;
        private readonly Objectif objectif;
        private string comment;
        private bool selectChanged;
        private bool status;

        #endregion Private Fields

        #region Public Constructors

        public OptitraiteViewModel(Objectif obj)
        {
            objectif = obj;
            var listComplements = objectif?.Complements;

            if (listComplements?.Count > 0)
            {
                var control = listComplements.FirstOrDefault(c => c.Id == 1); // Contrôle Optitraite annuel
                var bilan = listComplements.FirstOrDefault(c => c.Id == 2); // Bilan Pass Optitraite

                if (control != null)
                {
                    var contenu = control.Contenus.First();
                    controleVm = new ComplementViewModel(control.Id, contenu, control.Nom);
                    controleVm.RaiseEvent += HandleEvent;
                }

                if (bilan != null)
                {
                    var contenu = bilan.Contenus.First();
                    bilanVm = new ComplementViewModel(bilan.Id, contenu, bilan.Nom);
                    bilanVm.RaiseEvent += HandleEvent;
                }
            }

            Debug.WriteLine("");
        }

        #endregion Public Constructors

        #region Public Properties

        public ComplementViewModel Bilan => bilanVm;

        public string Comment
        {
            get { return comment; }
            set
            {
                comment = value;
                OnPropertyChanged("Comment");
            }
        }

        public ComplementViewModel Controle => controleVm;

        public string Nom => $"{objectif.Id} {objectif.Nom}";

        public bool Status
        {
            get
            {
                var state = controleVm.Status && bilanVm.Status;
                StatusMessage = state ? "Atteint" : "Non atteint";
                OnPropertyChanged("StatusMessage");
                return state;
            }
        }

        public string StatusMessage { get; set; }

        #endregion Public Properties

        #region Private Methods

        // Define what actions to take when the event is raised.
        private void HandleEvent(object sender, CustomEventArgs e)
        {
            Debug.WriteLine($"Complement changed : {e.Origine}");
            OnPropertyChanged("Status");
        }

        #endregion Private Methods
    }
}