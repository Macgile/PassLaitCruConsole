﻿// ReSharper disable CheckNamespace
// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable MemberCanBePrivate.Global
namespace WPFPassLaitCru.ViewModel
{
    using WPFModern;

    public abstract class ViewModel<TModel> : NotifyPropertyChanged
    {
        #region Constructors

        protected ViewModel(TModel model)
        {
            Model = model;
        }

        #endregion Constructors


        #region Properties

        public TModel Model {get;private set; }

        #endregion Properties

    }
}
