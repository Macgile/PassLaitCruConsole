﻿using System.Collections.Generic;
using WPFModern;

// ReSharper disable MemberCanBePrivate.Global

namespace WPFPassLaitCru
{
    public class Complement
    {
        #region Properties

        // List of Complements
        public List<List<Contenu>> Contenus { get; set; }
        // Count of Complement List
        public int Count => Contenus.Count;
        // Complement ID
        public int Id { get; set; }
        // Name of Complement
        public string Nom { get; set; }

        #endregion Properties

        #region Constructors

        public Complement()
        {
            Contenus = new List<List<Contenu>>();
        }

        #endregion Constructors
    }

    public class Contenu //: NotifyPropertyChanged
    {
        #region Properties

        public string Description { get; set; }
        public int Groupe { get; set; }
        public int Id { get; set; }
        public bool Realized { get; set; }

        #endregion Properties
    }

    public class Objectif
    {
        #region Constructors

        public Objectif()
        {
            Complements = new List<Complement>();
        }

        #endregion Constructors

        #region Properties

        public List<Complement> Complements { get; }
        public int Count => Complements.Count;
        public int Id { get; set; }
        public string Nom { get; set; }

        #endregion Properties

        #region Methods

        public void Add(Complement children)
        {
            Complements.Add(children);
        }

        //public IList<>

        #endregion Methods
    }
}