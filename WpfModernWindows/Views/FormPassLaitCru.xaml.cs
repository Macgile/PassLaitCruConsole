﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using WPFPassLaitCru.ViewModel;

// ReSharper disable UnusedVariable

namespace WPFPassLaitCru.Views
{
    /// <inheritdoc cref="UserControl" />
    /// <summary>
    /// Logique d'interaction pour FormPassLaitCru.xaml
    /// </summary>
    public partial class FormPassLaitCru
    {
        #region Public Constructors

        public FormPassLaitCru()
        {
            InitializeComponent();

            MainViewModel = new MainViewModel();
            MainViewModel.GetAll();
            DataContext = MainViewModel;
            Unloaded += OnUnloaded;
        }

        #endregion Public Constructors

        #region Private Properties

        private MainViewModel MainViewModel { get; set; }

        #endregion Private Properties

        #region Private Methods

        private void OnUnloaded(object sender, RoutedEventArgs e)
        {
            var ob = sender;
            Debug.WriteLine("");
        }

        #endregion Private Methods
    }
}