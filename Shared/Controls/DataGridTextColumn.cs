﻿using System.Windows;

namespace WPFModern.Controls
{
    /// <inheritdoc />
    /// <summary>
    /// A DataGrid text column using default Modern UI element styles.
    /// </summary>
    public class DataGridTextColumn
        : System.Windows.Controls.DataGridTextColumn
    {
        /// <inheritdoc />
        /// <summary>
        /// Initializes a new instance of the <see cref="T:WPFModern.Controls.DataGridTextColumn" /> class.
        /// </summary>
        public DataGridTextColumn()
        {
            ElementStyle = Application.Current.Resources["DataGridTextStyle"] as Style;
            EditingElementStyle = Application.Current.Resources["DataGridEditingTextStyle"] as Style;
        }
    }
}
