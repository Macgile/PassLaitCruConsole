﻿using System.Windows;

namespace WPFModern.Controls
{
    /// <inheritdoc />
    /// <summary>
    /// A DataGrid checkbox column using default Modern UI element styles.
    /// </summary>
    public class DataGridComboBoxColumn
        : System.Windows.Controls.DataGridComboBoxColumn
    {
        /// <inheritdoc />
        /// <summary>
        /// Initializes a new instance of the <see cref="T:WPFModern.Controls.DataGridComboBoxColumn" /> class.
        /// </summary>
        public DataGridComboBoxColumn()
        {
            this.EditingElementStyle = Application.Current.Resources["DataGridEditingComboBoxStyle"] as Style;
        }
    }
}
