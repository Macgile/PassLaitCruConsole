﻿using System.Windows;

namespace WPFModern.Controls
{
    /// <inheritdoc />
    /// <summary>
    /// A DataGrid checkbox column using default Modern UI element styles.
    /// </summary>
    public class DataGridCheckBoxColumn
        : System.Windows.Controls.DataGridCheckBoxColumn
    {
        /// <inheritdoc />
        /// <summary>
        /// Initializes a new instance of the <see cref="T:WPFModern.Controls.DataGridCheckBoxColumn" /> class.
        /// </summary>
        public DataGridCheckBoxColumn()
        {
            ElementStyle = Application.Current.Resources["DataGridCheckBoxStyle"] as Style;
            EditingElementStyle = Application.Current.Resources["DataGridEditingCheckBoxStyle"] as Style;
        }
    }
}
