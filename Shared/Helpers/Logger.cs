﻿// ReSharper disable CheckNamespace
namespace WPFModern
{
    using System;
    using System.IO;

    /// <summary>
    /// Gestion des log dans un fichier txt
    /// </summary>
    public static class Logger
    {
        /// <summary>
        /// Ecrit un message dans un fichier log
        /// </summary>
        /// <param name="message">message à ajouter dans me fichier</param>
        public static void LogFile(string message)
        {
            try
            {
                var path = Path.GetDirectoryName(new Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase)
                    .AbsolutePath);

                var date = DateTime.Now;

                var dateString = string.Format("Event occurred on {3} {0}:{1}:{2}\r\n",
                    date.Hour, date.Minute, date.Second, date.ToShortDateString());

                var log = $"{dateString}\t- {message}\r\n\r\n";

                if (!Directory.Exists(path))
                {
                    if (path != null) Directory.CreateDirectory(path);
                }

                File.AppendAllText(path + @"/log.txt", log);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw;
                
            }
        }
    }
}