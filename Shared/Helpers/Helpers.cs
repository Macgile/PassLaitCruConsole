﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;

// ReSharper disable CheckNamespace

namespace WPFModern
{
    /// <summary>
    /// Provide extension for manage exception occured
    /// </summary>
    public static class ExceptionExtensions
    {
        #region Public Properties

        public static int ErrorCount { get; set; }

        public static string LastError { get; set; }

        #endregion Public Properties

        #region Public Methods

        public static void LogException(this Exception ex, string origin = "")
        {
            var innerEx = ex.GetOriginalException().Message;
            origin = string.IsNullOrEmpty(origin) ? "Unknow" : origin;

            var line = ex.LineNumber();

            var inner = innerEx.Equals(ex.Message, StringComparison.OrdinalIgnoreCase) ? "no inner exception" : innerEx;

            var message = $"\r\n" +
                          $"{"Origin of Exception",-28}{": " + origin}\r\n" +
                          $"{"Message",-28}{": " + ex.Message}\r\n" +
                          $"{"Inner Exception",-28}{": " + inner}\r\n" +
                          $"{"Line",-28}{": " + line}";

#if DEBUG
            Debug.WriteLine(message);
#endif

            LastError = message;

            // Log message into log file
            Logger.LogFile(message);

            ErrorCount++;
        }

        private static Exception GetOriginalException(this Exception ex)
        {
            return ex.InnerException == null ? ex : ex.InnerException.GetOriginalException();
        }

        private static int LineNumber(this Exception ex)
        {
            var lineNumber = 0;
            const string lineSearch = ":line ";
            var index = ex.StackTrace.LastIndexOf(lineSearch, StringComparison.Ordinal);

            if (index == -1) return lineNumber;

            var lineNumberText = ex.StackTrace.Substring(index + lineSearch.Length);
            if (int.TryParse(lineNumberText, out lineNumber))
            {
            }
            return lineNumber;
        }

        #endregion Public Methods
    }

    //Extension methods must be defined in a static class
    public static class StringExtension
    {
        #region Methods

        public static string FormatPhone(this string number)
        {
            if (number == null)
                return string.Empty;

            number = Regex.Replace(number, @"\s+", "");
            return number.Length > 0 ? string.Join(" ", Regex.Split(number, @"(\d{2})").Where(x => !string.IsNullOrWhiteSpace(x))) : number;
        }

        // This is the extension method.
        // The first parameter takes the "this" modifier
        // and specifies the type for which the method is defined.
        public static string TrimAndReduce(this string str)
        {
            return ConvertWhitespacesToSingleSpaces(str).Trim();
        }

        private static string ConvertWhitespacesToSingleSpaces(this string value)
        {
            return Regex.Replace(value, @"\s+", " ");
        }

        #endregion Methods
    }

    internal static class ClassExtensions
    {
        #region Public Methods

        /// <summary>
        /// Makes a copy from the object.
        /// Doesn't copy the reference memory, only data.
        /// </summary>
        /// <typeparam name="T">Type of the return object.</typeparam>
        /// <param name="item">Object to be copied.</param>
        /// <returns>Returns the copied object.</returns>
        public static T Clone<T>(this object item)
        {
            if (item != null)
            {
                var formatter = new BinaryFormatter();
                var stream = new MemoryStream();

                formatter.Serialize(stream, item);
                stream.Seek(0, SeekOrigin.Begin);

                T result = (T)formatter.Deserialize(stream);

                stream.Close();

                return result;
            }
            else
                return default(T);
        }

        public static T DeepCopy<T>(T obj)
        {
            using (var stream = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, obj);
                stream.Position = 0;

                return (T)formatter.Deserialize(stream);
            }
        }

        #endregion Public Methods
    }
}