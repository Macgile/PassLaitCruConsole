﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SqlItePassLaitCru.Models;

namespace SqlItePassLaitCru
{
    internal class Program
    {
        #region Private Methods

        static void Main(string[] args)
        {
            using (var db = new SqlIteEntities())
            {
                var exploitation = db.COMPLEMENT.Where(c => c.ID > 0).ToList();

                foreach (var item in exploitation)
                {
                    Debug.WriteLine(item.LIBELLE);
                }
            }
        }

        #endregion Private Methods
    }
}